/* eslint no-shadow: ["error", { "allow": ["state"] }] */
const state = {};

const getters = {};

const actions = {
  transferMember({ commit, rootState }, payload) {
    const rootStateReference = rootState;
    commit('transferMember', { payload, rootStateReference });
  },
};

const mutations = {
  transferMember(state, { payload, rootStateReference }) {
    const teams = rootStateReference.team.state.teams;
    let matchFound = false;
    for (let i = 0; i < teams.length && !matchFound; i += 1) {
      const members = teams[i].members;
      for (let j = 0; j < members.length && !matchFound; j += 1) {
        const currentMemberID = members[j].id;
        if (currentMemberID === payload.memberID && i !== payload.teamID) {
          members.splice(j, 1);
          matchFound = true;
        }
      }
    }
    if (matchFound) {
      const newMemberObject = {};
      newMemberObject.id = payload.memberID;
      newMemberObject.firstName = payload.firstName;
      newMemberObject.lastName = payload.lastName;
      teams[payload.teamID].members.push(newMemberObject);
    }
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
