// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import Vuex from 'vuex';
import transfer from './transfer';
import team from './modules/team';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    transfer,
    team,
  },
});
