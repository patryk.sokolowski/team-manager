/* eslint no-shadow: ["error", { "allow": ["state"] }] */
const state = {
  state: {
    latestMemberID: 0,
    teams: [
      {
        teamName: 'Team 1',
        members: [
          {
            id: 1,
            firstName: 'Jan',
            lastName: 'Nowak',
          },
          {
            id: 2,
            firstName: 'Jakub',
            lastName: 'Michalczewski',
          },
        ],
      },
      {
        teamName: 'Team 2',
        members: [
          {
            id: 3,
            firstName: 'Piotr',
            lastName: 'Kowalski',
          },
          {
            id: 4,
            firstName: 'Mateusz',
            lastName: 'Fronczewski',
          },
        ],
      },
    ],
  },
};

const getters = {
  getLatestMemberID() {
    const teams = state.state.teams;
    for (let i = 0; i < teams.length; i += 1) {
      const members = teams[i].members;
      for (let j = 0; j < members.length; j += 1) {
        if (members[j].id > state.state.latestMemberID) {
          state.state.latestMemberID = members[j].id;
        }
      }
    }
    return state.state.latestMemberID;
  },
};

const actions = {
  async saveNewTeam({ commit }, payload) {
    const newTeamObject = {};
    newTeamObject.id = this.getLatestMemberID() + 1;
    newTeamObject.teamName = payload.teamName;
    newTeamObject.members = payload.members;
    commit('saveNewTeam', await newTeamObject);
  },
  removeTeam({ commit }, payload) {
    commit('removeTeam', payload);
  },
  async saveNewMember({ commit }, payload) {
    const newMemberID = getters.getLatestMemberID() + 1;
    commit('saveNewMember', await { payload, newMemberID });
  },
  removeMember({ commit }, payload) {
    commit('removeMember', payload);
  },
};

const mutations = {
  saveNewTeam(state, payload) {
    state.state.teams.push(payload);
  },
  removeTeam(state, payload) {
    state.state.teams.splice(payload.teamID, 1);
  },
  saveNewMember(state, { payload, newMemberID }) {
    const teamMembersList = state.state.teams[payload.teamID].members;
    const newMemberObject = {};
    newMemberObject.id = newMemberID;
    newMemberObject.firstName = payload.firstName;
    newMemberObject.lastName = payload.lastName;
    teamMembersList.push(newMemberObject);
  },
  removeMember(state, payload) {
    const membersList = state.state.teams[payload.teamID].members;
    for (let i = 0; i < membersList.length; i += 1) {
      if (membersList[i].id === payload.memberID) {
        membersList.splice(i, 1);
      }
    }
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
